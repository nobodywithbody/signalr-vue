# Signalr vue integration plugin

# How install this plugin with yarn

- if main OS is windows install mingw shell with bash or directly from [mingw](http://mingw.org/)
- [Gitlab ssh import tutorial](https://docs.gitlab.com/ce/ssh/README.html)
- [how configure your repo with nexus-register](http://wiki.moex.com/pages/viewpage.action?pageId=70527923)
- run `yarn add message-place`

# How contribute this plugin

- set env var `NEXUS_TOKEN` value is `base64` of `login:password` from your nexus account
- change version with `yarn version`
- add `tag` on the repo
- publish it with a command `npm publish`(yarn has problem)

# How use it

## With only Vue

- configure plugin in `plugins/vue-signalr.js`

```js
import Vue from 'vue';
import SignalrPlugin from 'message-place';

//base config
const config = {
  protocol: 'http://',
  host: 'localhost',
  port: '64684',
  path: '/eventHub'
};

Vue.use(SignalrPlugin, config);
```

## with nuxt.js

- connect plugin in `nuxt.config.js`

```json
{
  "plugins": ["~plugins/vue-websocket"]
}
```

# Plugin config options

- `protocol` - protocol of signalr host. _Default_ - `http://`
- `host` - host signalr server. _Default_ - `localhost`
- `port` - port signalr server. _Default_ - `44395`
- `path` - path url. _Default_ - `""`
- `reconnect` - auto reconnect if connection is lost. _Default_ - `false`
- `reconnectTimeout` - timeout in ms between reconnect attempts. _Default_ - `5000`
- `logLevel` - level of log detalization. For that use `LogLevel` enum from `@aspnet/signalr` library. _Default_ - `LogLevel.Trace`
- `autoconnect` - immediatly trying connect after use this plugin. For manualy run `this.$websocket.connect()` from component. _Default_ - `false`
- `WebsocketClass` - class implementation communications with signalr server. _Default_ - `WebsocketFacade`
- `transport` - default transports used by signalr. By default set _only_ `WebSocket` protocol
- `skipNegotiation` - skip negotiation request before establishing connection
- `accessTokenFactory` - function which return access token

# How use it in Vue

Public API located in `this.$websocket` property. You can use this plugin programmaticaly from Vue component in any lifecycle after `beforeCreate`

```js
{
  mounted() {
    this.$websocket.connect();
  }
}
```

Or use can set directive in `websocketEvents` property. Where key is name of event and value is method when called if this event send from server.

```js
{
  name: 'SomeComponent',
  websocketEvents: {
    testEvent() {
      console.log('this method was been called after receiving message from server')
    }
  }
}
```

## Public API implements next methods:

### `$websocket.connect(): Promise<T>` - connect to signalr server

### `$websocket.disconnect(): void` - disconnect from server

### `$websocket.on(eventName: string, listener: () => void): void` - add listener to event

### `$websocket.off(eventName: string, listener: () => void): void` - remove listener to event. You cannot programmaticaly remove listener added with property `websocket`

### `$websocket.invoke(methodName: string, args[any]): Promise<T>` - invoke method on server. Params must be is an Array.

### `$websocket.updateConnection(options): void` - dynamically set options. If server started at this moment it will stop automative. After that you need invoke `connect`, but if you pass `autoconnect` equal `true` in options, then client connect with signalr server automaticaly with new options

Also components has reactive property `$websocketState`. This change after change state of plugin. It may contain next statuses:

`stop` - stopped connection
`pending` - connection to server in progress
`failed` - connection to server lost with error
`success` - success connecting ti server at this moment

Because is property reactive you can use it in `watch` or in `computed` properties.

```js
import {STATE} from 'message-place.moex.com'
  {
    watch: {
      $websocketState(val) {
        if (val === STATE.SUCCESS) {
          console.info("connect to servser is success!")
        }
      }
    }
  }
```
