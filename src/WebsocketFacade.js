import { HubConnectionBuilder, LogLevel, HttpTransportType } from '@aspnet/signalr';

export const STATE = {
  STOP: 'stop',
  PENDING: 'pending',
  FAILED: 'failed',
  SUCCESS: 'success',
};

export class WebsocketFacade {
  constructor(options) {
    this.options = {
      url: '',
      logLevel: LogLevel.TRACE,
      reconnect: false,
      autoconnect: false,
      reconnectTimeout: 1000,
      skipNegotiation: true,
      transport: HttpTransportType.WebSockets,
    };
    this.preventReconnect = false;
    this.state = STATE.STOP;
    this.listeners = {};
    this.connection = null;
    this.setOptions(options);
  }

  setOptions(options) {
    // eslint-disable-line no-undef
    this.options = { ...this.options, ...options };
  }

  updateConnection(options) {
    if (options) {
      this.setOptions(options);
    }

    const {
      url,
      logLevel,
      autoconnect,
      skipNegotiation,
      transport,
      accessTokenFactory,
      reconnect,
    } = this.options;

    if (this.state === STATE.SUCCESS) {
      this.disconnect();
    }

    this.connection = new HubConnectionBuilder()
      .withUrl(url, {
        skipNegotiation,
        transport,
        accessTokenFactory,
      })
      .configureLogging(logLevel)
      .build();

    if (reconnect) {
      this.connection.onclose(this.afterDisconnected.bind(this));
    }

    if (autoconnect) {
      this.connect();
    }
  }

  connect() {
    this.state = STATE.PENDING;
    return this.connection
      .start()
      .then((response) => {
        this.state = STATE.SUCCESS;
        this.enableListeners();
        return response;
      })
      .catch(this.afterErrorDisconnected.bind(this));
  }

  enableListeners() {
    Object.keys(this.listeners).forEach(listener => this.addHandlerForEvent(listener));
  }

  afterErrorDisconnected(error) {
    console.error(`Disconnected with error ${JSON.stringify(error)}`);
    const { reconnectTimeout, reconnect } = this.options;
    if (reconnect) {
      console.info(`Autoreconnect is enabled. Reconnect after ${reconnectTimeout}ms`);
      this.reconnect(reconnectTimeout);
    }
  }

  afterDisconnected() {
    const { reconnect, reconnectTimeout } = this.options;
    if (reconnect && !this.preventReconnect) {
      this.reconnect(reconnectTimeout);
    }
    this.preventReconnect = false;
  }

  eventHandler(event) {
    return (...params) => {
      if (!this.listeners[event]) {
        return;
      }

      this.listeners[event].forEach((cb) => {
        cb(...params);
      });
    };
  }

  addHandlerForEvent(event) {
    // добавление работает только в случае если соединение утсановлено, иначе методы не вызываются
    if (this.state !== STATE.SUCCESS) {
      return;
    }
    this.connection.on(event, this.eventHandler(event));
  }

  disconnect() {
    this.preventReconnect = true;
    this.connection.stop();
    this.state = STATE.STOP;
  }

  reconnect(timeout) {
    setTimeout(() => {
      this.connect();
    }, timeout);
  }

  addListener(event, listener) {
    if (!this.listeners[event]) {
      this.addHandlerForEvent(event);
      this.listeners[event] = [];
    }

    this.listeners[event].push(listener);
  }

  removeListener(event, listener = null) {
    if (!this.listeners[event]) {
      return false;
    }

    if (!listener) {
      this.listeners[event] = [];
      return true;
    }

    this.listeners[event] = this.listeners[event].filter(cb => cb !== listener);
    return true;
  }

  invoke(message, params = []) {
    if (this.state !== STATE.SUCCESS) {
      return console.error('State is not stoped. Connect to server and try again.');
    }
    return this.connection.invoke(message, ...params);
  }
}
