import { LogLevel, HttpTransportType } from '@aspnet/signalr';
import { WebsocketFacade } from './WebsocketFacade';

let websocketFacade = null;
const defaultTransports = HttpTransportType.WebSockets;
const isServer = typeof window === 'undefined';

const filterObject = equalValue => obj => Object.keys(obj).reduce(
  (prev, cur) => (obj[cur] === equalValue ? prev : { ...prev, ...{ [cur]: obj[cur] } }),
  {},
);

export const publicInterface = {
  on(event, listener) {
    websocketFacade.addListener(event, listener);
  },
  off(event, listener) {
    websocketFacade.removeListener(event, listener);
  },
  connect() {
    return websocketFacade.connect();
  },
  disconnect() {
    return websocketFacade.disconnect();
  },
  invoke(method, ...rest) {
    return websocketFacade.invoke(method, rest);
  },
  updateConnection(options) {
    websocketFacade.updateConnection(options);
  },
};

const serverInterface = Object.keys(publicInterface).reduce(
  (prev, cur) => ({
    ...prev,
    [cur]: () => Promise.resolve(),
  }),
  {},
);

export * from './WebsocketFacade';
export default {
  install(
    Vue,
    {
      protocol = 'http://',
      host = 'localhost',
      port = '80',
      path = '',
      transport = defaultTransports,
      reconnect = false,
      reconnectTimeout = 5000,
      logLevel = LogLevel.Trace,
      autoconnect = false,
      WebsocketClass = WebsocketFacade,
      skipNegotiation = true,
      accessTokenFactory,
    } = {},
  ) {
    const url = `${protocol}${host}${port ? `:${port}` : ''}${path}`;
    const options = {
      url,
      logLevel,
      autoconnect,
      reconnect,
      reconnectTimeout,
      skipNegotiation,
      transport,
      accessTokenFactory,
    };
    const notEmptyOptions = filterObject(undefined)(options);
    websocketFacade = new WebsocketClass(notEmptyOptions);

    if (!isServer) {
      websocketFacade.updateConnection();
    }

    Vue.mixin({
      beforeCreate() {
        this._websocketFacade = websocketFacade;
        Vue.util.defineReactive(this, '_websocketFacade', websocketFacade);
        if (!this.$options.websocketEvents) {
          return;
        }

        let { websocketEvents } = this.$options;

        websocketEvents = Object.keys(websocketEvents).reduce(
          (prev, cur) => ({
            ...prev,
            [cur]: websocketEvents[cur].bind(this),
          }),
          {},
        );

        this.$options.websocketEvents = websocketEvents;

        Object.keys(websocketEvents).forEach((event) => {
          this.$websocket.on(event, websocketEvents[event]);
        });
      },
      destroyed() {
        const { websocketEvents = {} } = this.$options;
        Object.keys(websocketEvents).forEach((event) => {
          this.$websocket.off(event, websocketEvents[event]);
        });
      },
    });

    Vue.prototype.$websocket = isServer ? serverInterface : publicInterface; // eslint-disable-line

    if (!Vue.prototype.hasOwnProperty.call(Vue, '$websocketState')) {
      Object.defineProperty(Vue.prototype, '$websocketState', {
        get() {
          return this._websocketFacade && this._websocketFacade.state;
        },
      });
    }
  },
};
