import { WebsocketFacade, STATE } from '../src/WebsocketFacade';

const signalr = require('@aspnet/signalr');

describe('WebsocketFacade', () => {
  const withUrlMock = jest.fn();
  const configureLoggingMock = jest.fn();
  const buildMock = jest.fn();
  const oncloseMock = jest.fn();
  const startMock = jest.fn();
  const onMock = jest.fn();
  const stopMock = jest.fn();
  const invokeMock = jest.fn();

  beforeEach(() => {
    jest.mock('@aspnet/signalr');

    signalr.HubConnectionBuilder = jest.fn().mockImplementation(() => ({
      withUrl: withUrlMock.mockImplementation(function withUrl() {
        return this;
      }),
      configureLogging: configureLoggingMock.mockImplementation(function configureLogging() {
        return this;
      }),
      build: buildMock.mockImplementation(function build() {
        return this;
      }),
      onclose: oncloseMock,
      start: startMock,
      on: onMock,
      stop: stopMock,
      invoke: invokeMock,
    }));
  });

  afterEach(() => {
    jest.unmock('@aspnet/signalr');
    withUrlMock.mockClear();
    configureLoggingMock.mockClear();
    buildMock.mockClear();
    oncloseMock.mockClear();
    startMock.mockRestore();
    onMock.mockRestore();
    stopMock.mockRestore();
    invokeMock.mockRestore();
  });

  test('state const have lifecycle', () => ['STOP', 'PENDING', 'FAILED', 'SUCCESS'].forEach(stateKey => expect(STATE[stateKey]).toEqual(stateKey.toLowerCase())));

  test('correct create with empty attributes', () => {
    const facade = new WebsocketFacade();
    expect(facade).toHaveProperty('options');
    expect(facade.options).toEqual({
      url: '',
      logLevel: signalr.LogLevel.TRACE,
      reconnect: false,
      autoconnect: false,
      reconnectTimeout: 1000,
      skipNegotiation: true,
      transport: signalr.HttpTransportType.WebSockets,
    });
    expect(facade.preventReconnect).toEqual(false);
    expect(facade.state).toEqual(STATE.STOP);
    expect(facade.connection).toBeNull();
  });

  test('correct override default options in constructor', () => {
    const expectOptions = {
      url: 'http://test.ru',
      logLevel: '5',
      reconnect: true,
      autoconnect: true,
      reconnectTimeout: 9000,
      skipNegotiation: false,
      transport: 'test',
    };

    const facade = new WebsocketFacade(expectOptions);
    expect(facade.options).toEqual(expectOptions);
  });

  test('correct partial override default options', () => {
    const expectOptions = {
      url: 'http://test.ru',
      logLevel: '5',
      reconnectTimeout: 9000,
      skipNegotiation: false,
      transport: 'test',
    };

    const facade = new WebsocketFacade(expectOptions);

    expectOptions.reconnect = false;
    expectOptions.autoconnect = false;
    expect(facade.options).toEqual(expectOptions);
  });

  test('correct reconnecting if start failed', async () => {
    jest.useFakeTimers();
    startMock
      .mockRejectedValueOnce(new Error('one failure'))
      .mockRejectedValueOnce(new Error('two failure'))
      .mockResolvedValueOnce(true);

    const facade = new WebsocketFacade({ reconnect: true });
    facade.updateConnection();
    await facade.connect();
    jest.runAllTimers();
    await Promise.resolve();
    jest.runAllTimers();
    await Promise.resolve();

    expect(startMock).toHaveBeenCalledTimes(3);
    expect(facade.state).toEqual(STATE.SUCCESS);
    jest.useRealTimers();
  });

  describe('#updateConnection', () => {
    test('invoked set options', () => {
      const setOptionsMock = jest.fn();
      const facade = new WebsocketFacade();
      facade.setOptions = setOptionsMock;

      facade.updateConnection({
        skipNegotiation: true,
      });

      expect(setOptionsMock).toBeCalled();
      expect(setOptionsMock).toBeCalledWith({ skipNegotiation: true });
    });

    test('create new a connection with new options', () => {
      const disconnectMock = jest.fn();
      const accessTokenFactoryMock = jest.fn();

      const facade = new WebsocketFacade();
      facade.disconnect = disconnectMock;
      facade.updateConnection({
        url: 'test',
        accessTokenFactory: accessTokenFactoryMock,
      });

      expect(disconnectMock).toBeCalledTimes(0);
      expect(withUrlMock).toBeCalledWith('test', {
        accessTokenFactory: accessTokenFactoryMock,
        skipNegotiation: true,
        transport: 1,
      });
    });

    test('disconnect if a state equal started', () => {
      const disconnectMock = jest.fn();
      const facade = new WebsocketFacade();
      facade.disconnect = disconnectMock;
      facade.state = STATE.SUCCESS;

      facade.updateConnection();
      expect(disconnectMock).toBeCalledTimes(1);
    });

    test('connect after a build if autoconnect equal `true`', () => {
      const connectMock = jest.fn();
      const facade = new WebsocketFacade();
      facade.connect = connectMock;

      facade.updateConnection({
        autoconnect: true,
      });

      expect(connectMock).toBeCalledTimes(1);
      expect(oncloseMock).toBeCalledTimes(0);
    });

    test('set retrying a callback with an argument autoreconnect equal `true`', () => {
      const connectMock = jest.fn();
      const facade = new WebsocketFacade();
      facade.connect = connectMock;

      facade.updateConnection({
        reconnect: true,
      });

      expect(connectMock).toBeCalledTimes(0);
      expect(oncloseMock).toBeCalledTimes(1);
    });
  });

  describe('#connect', () => {
    test('connect invoking start a connection', async () => {
      startMock.mockResolvedValue(true);
      const facade = new WebsocketFacade();
      facade.updateConnection();

      const response = await facade.connect();
      expect(startMock).toBeCalledTimes(1);
      expect(response).toBeTruthy();
    });

    test('invoke afterDisconnect if connection rejected', async () => {
      const afterErrorDisconnectedMock = jest.fn();
      startMock.mockRejectedValue(new Error('connection not available'));

      const facade = new WebsocketFacade();
      facade.afterErrorDisconnected = afterErrorDisconnectedMock;

      facade.updateConnection();
      await facade.connect();

      expect(afterErrorDisconnectedMock).toBeCalledTimes(1);
    });
  });

  describe('#afterErrorDisconnected', () => {
    test('Not reconnect if an same option disabled', () => {
      const reconnectMock = jest.fn();
      const facade = new WebsocketFacade();
      facade.reconnect = reconnectMock;

      facade.afterErrorDisconnected();

      expect(reconnectMock).toBeCalledTimes(0);
    });

    test('Reconnect if the same option enable', () => {
      const reconnectMock = jest.fn();
      const facade = new WebsocketFacade({ reconnect: true });
      facade.reconnect = reconnectMock;
      facade.afterErrorDisconnected();

      expect(reconnectMock).toBeCalledTimes(1);
    });
  });

  describe('#afterDisconnected', () => {
    test('reconnect if option is enable', () => {
      const reconnectMock = jest.fn();

      const facade = new WebsocketFacade({ reconnect: true });
      facade.reconnect = reconnectMock;

      facade.afterDisconnected();
      expect(reconnectMock).toBeCalledTimes(1);
    });

    test('not reconnect if preventing connection', () => {
      const reconnectMock = jest.fn();

      const facade = new WebsocketFacade({ reconnect: true });
      facade.reconnect = reconnectMock;
      facade.preventReconnect = true;

      facade.afterDisconnected();
      expect(reconnectMock).toBeCalledTimes(0);
    });
  });

  describe('#eventHandler', () => {
    test('all callbacks are executed', () => {
      const eventCbOneMock = jest.fn();
      const eventCbTwoMock = jest.fn();

      const facade = new WebsocketFacade();
      facade.listeners = { testEvent: [eventCbOneMock, eventCbTwoMock] };

      facade.eventHandler('testEvent')('firstParam', 'secondParam');

      expect(eventCbOneMock).toBeCalledWith('firstParam', 'secondParam');
      expect(eventCbTwoMock).toBeCalledWith('firstParam', 'secondParam');
    });

    test('do not throw error if event not exist in listeners', () => {
      const facade = new WebsocketFacade();
      facade.eventHandler('missedEvent')();
    });
  });

  describe('#addHandlerForEvent', () => {
    test('add event to signalr listeners', () => {
      const cbExecutorMock = jest.fn();
      const facade = new WebsocketFacade();
      facade.eventHandler = jest.fn().mockImplementation(() => cbExecutorMock);
      facade.updateConnection();

      facade.addHandlerForEvent('event');
      expect(onMock).not.toHaveBeenCalledWith('event', cbExecutorMock);
      expect(facade.eventHandler).not.toHaveBeenCalledWith('event');
    });
  });

  describe('#diconnect', () => {
    test('correct stop connection', () => {
      const facade = new WebsocketFacade();
      facade.updateConnection();
      facade.disconnect();

      expect(stopMock).toBeCalledTimes(1);
      expect(facade.state).toEqual(STATE.STOP);
      expect(facade.preventReconnect).toBeTruthy();
    });
  });

  describe('#reconnect', () => {
    test('start timer with custom value of timeout', () => {
      jest.useFakeTimers();
      const connectMock = jest.fn();
      const facade = new WebsocketFacade();
      facade.connect = connectMock;

      facade.reconnect(600);
      jest.runAllTimers();

      expect(setTimeout.mock.calls[0][1]).toEqual(600);
      expect(connectMock).toHaveBeenCalled();
      jest.useRealTimers();
    });
  });

  describe('#addListener', () => {
    test('add missing event', () => {
      const cbMock = jest.fn();
      const addHandlerForEventMock = jest.fn();
      const facade = new WebsocketFacade();
      facade.addHandlerForEvent = addHandlerForEventMock;
      facade.addListener('test', cbMock);

      expect(facade.listeners).toHaveProperty('test');
      expect(addHandlerForEventMock).toHaveBeenCalledWith('test');
      expect(facade.listeners.test).toEqual([cbMock]);
    });
  });

  describe('#removeListener', () => {
    test('remove a specific listener', () => {
      const cbOneMock = jest.fn();
      const cbTwoMock = jest.fn();
      const facade = new WebsocketFacade();
      facade.listeners = { event: [cbOneMock, cbTwoMock] };
      facade.removeListener('event', cbTwoMock);

      expect(facade.listeners).toEqual({ event: [cbOneMock] });
    });

    test('remove all listeners at a custom event', () => {
      const cbOneMock = jest.fn();
      const facade = new WebsocketFacade();
      facade.listeners = { event: [cbOneMock] };

      facade.removeListener('event');
      expect(facade.listeners).toEqual({ event: [] });
    });

    test('do not throw error then remove missed event type', () => {
      const facade = new WebsocketFacade();

      facade.removeListener('event');
    });

    test('do not throw error then remove specific missed listener', () => {
      const facade = new WebsocketFacade();
      facade.listeners = { event: [] };

      facade.removeListener('event', jest.fn());
    });
  });

  describe('#invoke', () => {
    test('invoke method with params', () => {
      const facade = new WebsocketFacade();
      facade.updateConnection();
      facade.state = STATE.SUCCESS;
      facade.invoke('method', ['one', 'two']);

      expect(invokeMock).toHaveBeenCalledWith('method', 'one', 'two');
    });

    test('invoke with empty params', () => {
      const facade = new WebsocketFacade();
      facade.updateConnection();
      facade.state = STATE.SUCCESS;
      facade.invoke('method');

      expect(invokeMock).toHaveBeenCalledWith('method');
    });

    test('not invoke method if state do not equal success', () => {
      const facade = new WebsocketFacade();
      facade.updateConnection();
      facade.invoke('method');
      expect(invokeMock).not.toHaveBeenCalled();
    });
  });
});
