import { shallowMount, createLocalVue } from '@vue/test-utils';
import SignalrPlugin, { publicInterface } from '../src';

const Component = {
  template: '<div />',
};

const addListenerMock = jest.fn();
const updateConnectionMock = jest.fn();
const removeListenerMock = jest.fn();
const connectMock = jest.fn();
const disconnectMock = jest.fn();
const invokeMock = jest.fn();

const mockFacade = {
  state: 'test',
  updateConnection: updateConnectionMock,
  addListener: addListenerMock,
  removeListener: removeListenerMock,
  connect: connectMock,
  disconnect: disconnectMock,
  invoke: invokeMock,
};

jest.mock('../src/WebsocketFacade.js', () => ({
  WebsocketFacade: jest.fn().mockImplementation(() => mockFacade),
}));

const localVue = createLocalVue();
localVue.use(SignalrPlugin);

describe('Vue plugin', () => {
  beforeEach(() => {
    addListenerMock.mockRestore();
    removeListenerMock.mockRestore();
    connectMock.mockRestore();
    updateConnectionMock.mockRestore();
    disconnectMock.mockRestore();
    invokeMock.mockRestore();
  });

  it('inject public API', () => {
    const wrapper = shallowMount(Component, {
      localVue,
    });

    expect(Object.keys(wrapper.vm.$websocket)).toEqual([
      'on',
      'off',
      'connect',
      'disconnect',
      'invoke',
      'updateConnection',
    ]);

    expect(wrapper.vm.$websocketState).toEqual('test');
  });

  it('add event listeners before created', () => {
    const testListener = jest.fn();
    const onMock = jest.fn();
    const anotherLocalVue = createLocalVue();
    anotherLocalVue.use(SignalrPlugin);
    anotherLocalVue.prototype.$websocket = {
      on: onMock,
    };
    shallowMount(Component, {
      localVue: anotherLocalVue,
      websocketEvents: {
        test: testListener,
        testAnother: testListener,
      },
    });

    expect(onMock).toBeCalledTimes(2);
    expect(onMock.mock.calls[0][0]).toEqual('test');
    expect(onMock.mock.calls[1][0]).toEqual('testAnother');
  });

  it('remove listeners after destroy', () => {
    const testListener = jest.fn();
    const onMock = jest.fn();
    const offMock = jest.fn();
    const anotherLocalVue = createLocalVue();
    anotherLocalVue.use(SignalrPlugin);
    anotherLocalVue.prototype.$websocket = {
      on: onMock,
      off: offMock,
    };
    const wrapper = shallowMount(Component, {
      localVue: anotherLocalVue,
      websocketEvents: {
        test: testListener,
        testAnother: testListener,
      },
    });
    wrapper.destroy();
    expect(offMock).toBeCalledTimes(2);
    expect(onMock).toBeCalledTimes(2);
    expect(onMock.mock.calls[0][0]).toEqual('test');
    expect(onMock.mock.calls[1][0]).toEqual('testAnother');
  });

  test('websocketState is reactive', () => {
    const wrapper = shallowMount(Component, {
      localVue,
    });

    mockFacade.state = 'hello';
    expect(wrapper.vm.$websocketState).toEqual('hello');
  });

  test('public interface invoking methods of the facade', () => {
    const mockCb = jest.fn();
    publicInterface.on('testOn', mockCb);
    publicInterface.off('testOff', mockCb);
    publicInterface.connect();
    publicInterface.disconnect();
    publicInterface.invoke('testInvoke', 'param1', 'param2');
    publicInterface.updateConnection({ option: true });

    expect(addListenerMock).toBeCalledWith('testOn', mockCb);
    expect(removeListenerMock).toBeCalledWith('testOff', mockCb);
    expect(connectMock).toBeCalledTimes(1);
    expect(disconnectMock).toBeCalledTimes(1);
    expect(invokeMock).toHaveBeenCalledWith('testInvoke', ['param1', 'param2']);
    expect(updateConnectionMock).toHaveBeenCalledWith({ option: true });
  });
});
